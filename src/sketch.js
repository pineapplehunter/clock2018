/// <reference path="../p5.global-mode.d.ts" />

function setup() {
    createCanvas(window.innerWidth, window.innerHeight)
    // background(0,0,0)
    console.log(month(), day(), hour(), minute(), second())
    console.log(nowPercentage())
}

function nowPercentage() {
    if(moment() < moment("2019-01-01")){
        return (moment() - moment("2018-01-01")) / (moment("2019-01-01") - moment("2018-01-01"))
    }
    return (moment() - moment("2019-01-01")) / (moment("2020-01-01") - moment("2019-01-01"))
}

function draw() {
    background(0, 0, 0)
    translate(windowWidth / 2, windowHeight / 2)
    let scale = min(windowHeight, windowWidth) / 100
    
    // let month = () => 1
    // let day = () => 1
    // let hour = () => 0
    // let minute = () => 0
    // let second = () => 0

    // let month = () => 12
    // let day = () => 31
    // let hour = () => 23
    // let minute = () => 59
    // let second = () => 59

    let np = nowPercentage()
    fill(255, 255, 255)
    rect(-scale * 50, -scale * 50, scale * 100 * ((month()-1)/12+(day()-1)/(31*12)), scale * 20)
    fill(255,0,100)
    rect(-scale * 50, -scale * 30, scale * 100 * ((day()-1)/31+(hour())/(24*31)), scale * 20)
    fill(0,0,255)
    rect(-scale * 50, -scale * 10, scale * 100 * ((hour())/24+(minute())/(60*24)), scale * 20)
    fill(255,255,0)
    rect(-scale * 50, scale * 10, scale * 100 * ((minute())/60+(second())/(60*60)), scale * 20)
    fill(0,255,0)
    rect(-scale * 50, scale * 30, scale * 100 * ((second())/60), scale * 20)
    
    fill(200,200,200)
    textSize(15*scale)
    text(year().toString(), -scale * 40, -scale*25, scale * 80, scale*20)
    text((np * 100).toFixed(6).toString() + "%", -scale * 40, -scale*5, scale * 80, scale*20)
}

const now_max = 12 * 30 * 24 * 60 * 60

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}